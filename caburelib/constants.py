import os

from enum import Enum

# General purpose constants

CPU_COUNT = len(os.sched_getaffinity(0))


class FileFormats(Enum):
    JSON = "json"
    CSV = "csv"
    PYTHON_DICT = "python-dict"
    PARQUET = "parquet"
    GRIB = "grib"


UTC_TIMEZONE = "UTC"
DUMP_IN_RDS = True
DATETIME_FMT = "%Y-%m-%dT%H:%M:%S"
DATE_FMT = "%Y-%m-%d"

# Datasets
ARABLE = "arable"
DAVIS = "davis"
DROPPER = "dropper"
INTA = "ws_inta"
MANUAL = "manual"
METEOSTAR = "meteostar"
METOS = "metos"
NOAA = "noaa"
OMIXOM = "omixom"
GARFIN = "garfin"
INMET_BRAZIL = "inmet_brazil"
PLUVIOMANOS = "pluviomanos"
PLUVIOMANOS_CBA = "pluviomanos-cordoba"
SIAR = "siar"
SMN = "smn"
WEATHERBIT = "weatherbit"
WEATHERCOMPANY = "weathercompany"
YR_FORECAST = "yr-forecast"


class Datasources(Enum):
    ARABLE = ARABLE
    DAVIS = DAVIS
    DROPPER = DROPPER
    INTA = INTA
    MANUAL = MANUAL
    METEOSTAR = METEOSTAR
    METOS = METOS
    NOAA = NOAA
    OMIXOM = OMIXOM
    GARFIN = GARFIN
    INMET_BRAZIL = INMET_BRAZIL
    PLUVIOMANOS = PLUVIOMANOS
    PLUVIOMANOS_CBA = PLUVIOMANOS_CBA
    SIAR = SIAR
    SMN = SMN
    WEATHERBIT = WEATHERBIT
    WEATHERCOMPANY = WEATHERCOMPANY
    YR_FORECAST = YR_FORECAST


# datalake layers
DATALAKE_RAW = "cabure-datalake-raw"
DATALAKE_PARSED = "cabure-datalake-parsed"
DATALAKE_AGG = "cabure-datalake-aggregated"
DATALAKE_CALCULATED = "datalake-calculated"
