import json

from base64 import b64decode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from io import BytesIO, FileIO
from phpserialize import unserialize


def decrypt_pass(key, password):
    p_obj = json.loads(b64decode(password).decode())
    decobj = AES.new(key.encode(), AES.MODE_CBC, b64decode(p_obj["iv"]))
    data = decobj.decrypt(b64decode(p_obj["value"]))
    return unserialize(unpad(data, 16)).decode()


def get_partitions_from_key(key: str) -> dict:
    return {
        part.split("=")[0]: (part.split("=")[1] if len(part.split("=")) > 1 else None)
        for part in key.split("/")
    }


def json_to_jsonl(data: list) -> bytes:
    with BytesIO() as fp:
        for jsonline in data:
            fp.write(json.dumps(jsonline).encode("utf-8"))
            fp.write("\n".encode("utf-8"))
        return fp.getvalue()


def jsonl_to_json(jsonl_data: FileIO) -> list:
    return [json.loads(jline) for jline in jsonl_data.read().splitlines()]