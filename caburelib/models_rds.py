from enum import Enum

from sqlalchemy import (
    Column,
    Date,
    DateTime,
    Float,
    ForeignKey,
    Integer,
    Index,
    String,
    TIMESTAMP,
    text,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.schema import Index
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, CHAR
from sqlalchemy.orm import relationship

Base = declarative_base()
metadata = Base.metadata


class SensorServiceTypes(Enum):
    ARABLE = "App\\Models\\Stations\\Arable\\ArableNode"
    DAVIS = "App\\Models\\Stations\\Davis\\DavisNode"
    DROPPER = "App\\Models\\Stations\\Dropper\\DropperNode"
    METEOSTAR = "App\\Models\\Stations\\Meteostar\\MeteostarNode"
    METOS = "App\\Models\\Stations\\Metos\\MetosNode"


class NodableTypes(Enum):
    SIAR = "App\\Models\\Siar\\SiarNode"
    MANUAL = "App\\Models\\ManualNode"
    PALENQUE = "App\\Models\\PalenqueNode"
    PLUVIOMANO = "App\\Models\\PluviomanoNode"
    SENSOR = "App\\Models\\SensorNode"
    SMN = "App\\Models\\SmnNode"
    GPCBA = "App\\Models\\Stations\\GPCBA\\GPCBANode"
    INMET_BRAZIL = "App\\Models\\Stations\\InmetBrazil\\InmetBrazilNode"
    INTA = "App\\Models\\Stations\\INTA\\INTANode"
    WEATHERBIT = "App\\Models\\Stations\\Weatherbit\\WeatherbitNode"


class ArableUsers(Base):
    __tablename__ = "arable_users"
    id = Column(Integer, primary_key=True)
    user_email = Column(String(255))
    password = Column(String(255))
    api_key = Column(String(255))
    company_id = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class MetosUsers(Base):
    __tablename__ = "metos_users"
    id = Column(Integer, primary_key=True)
    public_key = Column(String(255))
    private_key = Column(String(255))
    company_id = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class MetosNodes(Base):
    __tablename__ = "metos_nodes"

    id = Column(Integer, primary_key=True)
    station_id = Column(String(255))
    device_name = Column(String(255))
    device_id = Column(String(255))
    uid = Column(String(255))
    firmware = Column(String(255))
    hardware = Column(String(255))
    description = Column(String(255))
    measuring_interval = Column(Integer)
    timezone_offset = Column(Integer)
    metos_user_id = Column(Integer, ForeignKey(f"{MetosUsers.__tablename__}.id"))
    status = Column(Integer)
    enabled = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    metos_user = relationship(MetosUsers.__name__)


class SiarNodes(Base):
    __tablename__ = "siar_nodes"
    id = Column(Integer, primary_key=True)
    ext_id = Column(String(255))
    name = Column(String(255))
    province = Column(String(255))
    community = Column(String(255))
    yutm = Column(Float)
    xutm = Column(Float)
    huso = Column(Float)
    alt = Column(Float)
    status = Column(Integer)
    enabled = Column(Integer)
    disconnected_at = Column(DateTime)
    installed_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class DropperNodes(Base):
    __tablename__ = "dropper_nodes"
    id = Column(Integer, primary_key=True)
    ext_id = Column(String(255))
    user = Column(String(255))
    password = Column(String(255))
    status = Column(Integer)
    enabled = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class GPCBANodes(Base):
    __tablename__ = "gpcba_nodes"
    id = Column(Integer, primary_key=True)
    ext_id = Column(String(191))
    name = Column(String(191))
    type = Column(String(191))
    alt = Column(Float)
    enabled = Column(Integer)
    status = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class InmetBrazilNodes(Base):
    __tablename__ = "inmet_brazil_nodes"
    id = Column(Integer, primary_key=True)
    ext_id = Column(String(191))
    name = Column(String(191))
    type = Column(String(191))
    alt = Column(Float)
    enabled = Column(Integer)
    status = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)



class DavisNodes(Base):
    __tablename__ = "davis_nodes"
    id = Column(Integer, primary_key=True)
    davis_username = Column(String(255))
    davis_password = Column(String(255))
    device_id = Column(String(255))
    device_key = Column(String(255))
    fetch_data_from_api = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)


class ArableNodes(Base):
    __tablename__ = "arable_nodes"
    id = Column(Integer, primary_key=True)
    ext_id = Column(Integer)
    owner_id = Column(Integer)
    sync_interval = Column(Integer)
    name = Column(String(255))
    last_deploy = Column(DateTime)
    org_id = Column(String(255))
    state = Column(String(255))
    firmware_id = Column(String(255))
    reported_fw = Column(String(255))
    signal_strength = Column(String(255))
    batt_volt = Column(Integer)
    timeoffset = Column(Integer)
    batt_pct = Column(Float)
    model = Column(String(255))
    last_seen = Column(DateTime)
    last_post = Column(DateTime)
    type = Column(String(255))
    arable_user_id = Column(Integer, ForeignKey(f"{ArableUsers.__tablename__}.id"))
    status = Column(Integer)
    enabled = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)

    arable_user = relationship(ArableUsers.__name__)


class SensorNodes(Base):
    __tablename__ = "sensor_nodes"
    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    elevation = Column(Float)
    KT = Column(Float)
    brand = Column(String(255))
    model = Column(String(255))
    version = Column(String(255))
    # FK to arable_nodes, davis_nodes, dropper_nodes, meteostar_nodes, metos_nodes
    serviceable_id = Column(String(255))
    # SensorServiceTypes
    serviceable_type = Column(String(255))
    status = Column(String(255))
    comments = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)
    company_id = Column(Integer)
    public = Column(Integer)


class Node(Base):
    __tablename__ = "nodes"
    __table_args__ = (
        Index("nodable_unique", "nodable_id", "nodable_type", unique=True),
    )

    guid = Column(String(255, "utf8_unicode_ci"), nullable=False, unique=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    updated_at = Column(DateTime)
    created_at = Column(DateTime)
    deleted_at = Column(DateTime)
    # FK sensor_nodes, gpcba_nodes, weatherbit_nodes, pluviomano_nodes, smn_nodes, siar_nodes, inta_nodes
    nodable_id = Column(INTEGER(11), nullable=False, index=True)
    # NodableTypes
    nodable_type = Column(String(255, "utf8_unicode_ci"), nullable=False, index=True)
    temperature = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    rainfall = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    relative_humidity = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    wind = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    air_pressure = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    ground_humidity = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    ground_temperature = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    radiation = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    dew_point = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    leave_humidity = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    chill_hours = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    frost = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    battery = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    daily_rainfall = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    window_day_rainfall = Column(TINYINT(1), nullable=False, server_default=text("'0'"))
    lat = Column(Float(asdecimal=True), nullable=False)
    lon = Column(Float(asdecimal=True), nullable=False)
    status = Column(INTEGER(11), nullable=False, server_default=text("'1'"))
    notify = Column(INTEGER(11), nullable=False, server_default=text("'1'"))
    public = Column(INTEGER(11), nullable=False, server_default=text("'0'"))
    country = Column(String(255, "utf8_unicode_ci"))
    administrative_area_level_1 = Column(String(255, "utf8_unicode_ci"))
    administrative_area_level_2 = Column(String(255, "utf8_unicode_ci"))
    formatted_address = Column(String(255, "utf8_unicode_ci"))
    last_data = Column(DateTime)
    administrative_area_level_3 = Column(String(255, "utf8_unicode_ci"))
    id = Column(INTEGER(10), primary_key=True)
    temperature_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'C'")
    )
    rainfall_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'mm'")
    )
    relative_humidity_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'%%'")
    )
    wind_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'km/h'")
    )
    air_pressure_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'hPa'")
    )
    ground_humidity_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'%%'")
    )
    ground_temperature_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'C'")
    )
    radiation_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'W/m2'")
    )
    dew_point_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("'C'")
    )
    leave_humidity_unit = Column(
        String(255, "utf8_unicode_ci"), nullable=False, server_default=text("''")
    )
    logo = Column(String(191, "utf8_unicode_ci"))
    ext_guid = Column(String(100, "utf8_unicode_ci"))
    wind_dir_unit = Column(String(255, "utf8_unicode_ci"), nullable=False)


class NodeDailyData(Base):
    __tablename__ = "daily_nodes_data"
    id = Column(Integer, primary_key=True)
    acc_rainfall = Column(Float)
    acc_gdd = Column(Float)
    avg_temperature = Column(Float)
    max_temperature = Column(Float)
    min_temperature = Column(Float)
    avg_humidity = Column(Float)
    max_humidity = Column(Float)
    min_humidity = Column(Float)
    avg_pressure = Column(Float)
    max_pressure = Column(Float)
    min_pressure = Column(Float)
    avg_wind = Column(Float)
    max_wind = Column(Float)
    min_wind = Column(Float)
    avg_radiation = Column(Float)
    max_radiation = Column(Float)
    min_radiation = Column(Float)
    avg_ground_temp = Column(Float)
    max_ground_temp = Column(Float)
    min_ground_temp = Column(Float)
    avg_ground_hum = Column(Float)
    max_ground_hum = Column(Float)
    min_ground_hum = Column(Float)
    et0_table = Column(Float)
    et0_measured = Column(Float)
    data_date = Column(Date, nullable=False, index=True)
    node_id = Column(
        Integer, ForeignKey(f"{Node.__tablename__}.id"), nullable=False, index=True
    )
    max_temperature_at = Column(DateTime)
    min_temperature_at = Column(DateTime)
    max_humidity_at = Column(DateTime)
    min_humidity_at = Column(DateTime)
    max_pressure_at = Column(DateTime)
    min_pressure_at = Column(DateTime)
    max_wind_at = Column(DateTime)
    min_wind_at = Column(DateTime)
    max_radiation_at = Column(DateTime)
    min_radiation_at = Column(DateTime)
    max_ground_temp_at = Column(DateTime)
    min_ground_temp_at = Column(DateTime)
    max_ground_hum_at = Column(DateTime)
    min_ground_hum_at = Column(DateTime)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    node = relationship(Node.__name__)
    daily_nodes_data_date_node_unique = Index(
        "daily_nodes_data_date_node_unique", data_date, node_id
    )


class NodeDashData(Base):
    __tablename__ = "nodes_dash_data"
    id = Column(Integer, primary_key=True)
    date_calculated = Column(DateTime)
    node_guid = Column(String(255), ForeignKey(f"{Node.__tablename__}.guid"))
    temperature = Column(Float)
    rainfall = Column(Float)
    rainfall_acc = Column(Float)
    hail = Column(Integer)
    relative_humidity = Column(Float)
    wind = Column(String(255))
    wind_intensity = Column(Float)
    wind_direction = Column(String(10))
    air_pressure = Column(Float)
    ground_humidity = Column(Float)
    ground_temperature = Column(Float)
    radiation = Column(Float)
    dew_point = Column(Float)
    ETo = Column(Float)
    leave_humidity = Column(Float)
    chill_hours = Column(Integer)
    damp_bulb_temperature = Column(Float)
    frost = Column(Integer)
    battery = Column(Float)

    node = relationship(Node.__name__)


class NodeDashDataHistory(Base):
    __tablename__ = "nodes_dash_data_history"
    id = Column(Integer, primary_key=True)
    date_calculated = Column(DateTime)
    node_guid = Column(String(255), ForeignKey(f"{Node.__tablename__}.guid"))
    temperature = Column(Float)
    rainfall = Column(Float)
    rainfall_acc = Column(Float)
    hail = Column(Integer)
    relative_humidity = Column(Float)
    wind = Column(String(255))
    wind_intensity = Column(Float)
    wind_direction = Column(String(10))
    air_pressure = Column(Float)
    ground_humidity = Column(Float)
    ground_temperature = Column(Float)
    radiation = Column(Float)
    dew_point = Column(Float)
    ETo = Column(Float)
    leave_humidity = Column(Float)
    chill_hours = Column(Integer)
    damp_bulb_temperature = Column(Float)
    frost = Column(Integer)
    battery = Column(Float)

    node = relationship(Node.__name__)


class User(Base):
    __tablename__ = "users"
    __table_args__ = (
        Index("users_email_deleted_at_unique", "email", "deleted_at", unique=True),
        Index("users_name_deleted_at_unique", "name", "deleted_at", unique=True),
    )

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"))
    email = Column(String(255, "utf8_unicode_ci"), nullable=False)
    password = Column(String(60, "utf8_unicode_ci"), nullable=False)
    remember_token = Column(String(100, "utf8_unicode_ci"))
    demo_duration_days = Column(INTEGER(11), server_default=text("'120'"))
    created_at = Column(
        TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'")
    )
    updated_at = Column(
        TIMESTAMP, nullable=False, server_default=text("'0000-00-00 00:00:00'")
    )
    company_id = Column(ForeignKey(f"companies.id"), index=True)
    confirmed = Column(INTEGER(1), server_default=text("'0'"))
    confirmation_code = Column(String(100, "utf8_unicode_ci"), nullable=False)
    temp_mail = Column(String(255, "utf8_unicode_ci"))
    mail_confirmation_code = Column(String(255, "utf8_unicode_ci"))
    is_from_app = Column(TINYINT(4), server_default=text("'0'"))
    deleted_at = Column(DateTime)

    company = relationship("Company", primaryjoin="User.company_id == Company.id")


class Company(Base):
    __tablename__ = "companies"
    __table_args__ = (
        Index("companies_name_deleted_at_unique", "name", "deleted_at", unique=True),
    )

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"))
    email = Column(String(255, "utf8_unicode_ci"))
    admin_user_id = Column(ForeignKey(f"{User.__tablename__}.id"), index=True)
    created_at = Column(DateTime)
    deleted_at = Column(DateTime)
    updated_at = Column(DateTime)

    admin_user = relationship(
        User.__name__, primaryjoin="Company.admin_user_id == User.id"
    )


class EstablishmentZone(Base):
    __tablename__ = "establishment_zones"

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    company_id = Column(ForeignKey(f"{Company.__tablename__}.id"), index=True)

    company = relationship(Company.__name__)


class GduDataConfig(Base):
    __tablename__ = "gdu_data_configs"

    id = Column(INTEGER(10), primary_key=True)
    region_name = Column(String(255, "utf8_unicode_ci"))
    node_id = Column(ForeignKey(f"{Node.__tablename__}.id"), nullable=False, index=True)
    enabled = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    hour_boundary = Column(INTEGER(11))
    base_temp = Column(INTEGER(11))
    max_temp = Column(INTEGER(11))
    sample_frequency = Column(INTEGER(11))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    valid_data_from_date = Column(TIMESTAMP)

    node = relationship(Node.__name__)


class GduDataReport(Base):
    __tablename__ = "gdu_data_reports"

    id = Column(INTEGER(10), primary_key=True)
    company_id = Column(
        ForeignKey(f"{Company.__tablename__}.id"), nullable=False, index=True
    )
    enabled = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    gdu_data_summaries = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    gdu_active_series = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    mails_to = Column(String(255, "utf8_unicode_ci"), nullable=False)
    gdu_data_summary_ids = Column(String(255, "utf8_unicode_ci"))
    gdu_data_series_ids = Column(String(255, "utf8_unicode_ci"))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)
    start_date = Column(TIMESTAMP)

    company = relationship(Company.__name__)


class ProductionType(Base):
    __tablename__ = "production_types"

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    company_id = Column(ForeignKey(f"{Company.__tablename__}.id"), index=True)

    company = relationship(Company.__name__)


class Subsidiary(Base):
    __tablename__ = "subsidiaries"

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    company_id = Column(
        ForeignKey(f"{Company.__tablename__}.id"), nullable=False, index=True
    )
    lastname = Column(String(255, "utf8_unicode_ci"))
    type = Column(TINYINT(4), nullable=False)
    tax_category = Column(INTEGER(11))
    tax_id_type = Column(String(255, "utf8_unicode_ci"))
    tax_id = Column(String(255, "utf8_unicode_ci"))
    street = Column(String(255, "utf8_unicode_ci"))
    door_number = Column(INTEGER(11))
    floor = Column(INTEGER(11))
    apartment_number = Column(String(255, "utf8_unicode_ci"))
    city = Column(String(255, "utf8_unicode_ci"))
    state = Column(String(255, "utf8_unicode_ci"))
    country = Column(String(255, "utf8_unicode_ci"))
    cp = Column(String(255, "utf8_unicode_ci"))
    main_phone = Column(String(255, "utf8_unicode_ci"))
    mobile_phone = Column(String(255, "utf8_unicode_ci"))
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime)
    created_at = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))

    company = relationship(Company.__name__)


class Establishment(Base):
    __tablename__ = "establishments"

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    subsidiary_id = Column(
        ForeignKey(f"{Subsidiary.__tablename__}.id"), nullable=False, index=True
    )
    establishment_zone_id = Column(
        ForeignKey(f"{EstablishmentZone.__tablename__}.id"), index=True
    )
    production_type_id = Column(
        ForeignKey(f"{ProductionType.__tablename__}.id"), index=True
    )
    productive_area = Column(Float(8, True))
    deleted_at = Column(DateTime)
    created_at = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    updated_at = Column(DateTime)

    establishment_zone = relationship(EstablishmentZone.__name__)
    production_type = relationship(ProductionType.__name__)
    subsidiary = relationship(Subsidiary.__name__)


class Share(Base):
    __tablename__ = "shares"

    id = Column(INTEGER(10), primary_key=True)
    name = Column(String(255, "utf8_unicode_ci"), nullable=False)
    area_coords = Column(String(5000, "utf8_unicode_ci"), nullable=False)
    establishment_id = Column(
        ForeignKey(f"{Establishment.__tablename__}.id", ondelete="CASCADE"),
        nullable=False,
        index=True,
    )
    area = Column(Float(8, True))
    perimeter = Column(Float(8, True))
    average_elevation = Column(Float(8, True))
    soil_type = Column(String(255, "utf8_unicode_ci"))
    deleted_at = Column(DateTime)
    created_at = Column(DateTime, server_default=text("CURRENT_TIMESTAMP"))
    updated_at = Column(DateTime)

    establishment = relationship(Establishment.__name__)


class GduSeriesConfig(Base):
    __tablename__ = "gdu_series_configs"

    id = Column(INTEGER(10), primary_key=True)
    uuid = Column(CHAR(36, charset="utf8_unicode_ci"), nullable=False, unique=True)
    gdu_data_config_id = Column(
        ForeignKey(f"{GduDataConfig.__tablename__}.id"), nullable=False, index=True
    )
    gdu_surrogate_data_config_id = Column(
        ForeignKey(f"{GduDataConfig.__tablename__}.id"), index=True
    )
    company_id = Column(
        ForeignKey(f"{Company.__tablename__}.id"), nullable=False, index=True
    )
    share_id = Column(ForeignKey(f"{Share.__tablename__}.id"), index=True)
    share_name = Column(String(255, "utf8_unicode_ci"))
    establishment_name = Column(String(255, "utf8_unicode_ci"))
    region_name = Column(String(255, "utf8_unicode_ci"))
    country_name = Column(String(255, "utf8_unicode_ci"))
    sowing_date = Column(Date, nullable=False)
    harvest_date = Column(Date)
    enabled = Column(TINYINT(1), nullable=False, server_default=text("'1'"))
    created_at = Column(TIMESTAMP)
    updated_at = Column(TIMESTAMP)

    company = relationship(Company.__name__)
    gdu_data_config = relationship(
        GduDataConfig.__name__, foreign_keys=[gdu_data_config_id]
    )
    gdu_surrogate_data_config = relationship(
        GduDataConfig.__name__, foreign_keys=[gdu_surrogate_data_config_id]
    )
    share = relationship(Share.__name__)
