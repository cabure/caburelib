from math import ceil, floor, isnan

import numpy as np
import pandas as pd

FARENHEIT_DEGREES = ["F", "°F"]
KELVIN_DEGREES = ["K", "°K"]
METERS_PER_SECOND = ["m/s"]
MILES_PER_HOUR = ["mph"]
FEET_PER_SECOND = ["ft/s", "ftps"]
KNOTS = ["knots", "kts"]
KILO_PASCAL = ["kp"]
MILIBAR = ["mb"]
MEGA_WATTS_PER_SQUARE_METER = ["MW/m2"]
MEGA_JOULES_PER_SQUARE_METER = ["MJ/m2"]
JOULES_PER_SQUARE_METER = ["J/m2"]
DEGREES = ["deg", "°"]
ES_COMPASS = ["es_compass"]
HOURS = ["h", "hs", "hours", "hour"]


def normalize_temperature(
    temperature_unit: str, data: pd.DataFrame, column: str
) -> pd.DataFrame:
    if temperature_unit in FARENHEIT_DEGREES:
        data[column] = data[column].apply(temp_f_to_c)
    if temperature_unit in KELVIN_DEGREES:
        data[column] = data[column].apply(temp_k_to_c)


def normalize_wind(wind_unit: str, data: pd.DataFrame, column: str) -> pd.DataFrame:
    if wind_unit in METERS_PER_SECOND:
        data[column] = data[column].apply(speed_mps_to_kmh)
    if wind_unit in MILES_PER_HOUR:
        data[column] = data[column].apply(speed_mph_to_kmh)
    if wind_unit in FEET_PER_SECOND:
        data[column] = data[column].apply(speed_fps_to_kmh)
    if wind_unit in KNOTS:
        data[column] = data[column].apply(speed_kts_to_kmh)


def normalize_pressure(
    air_pressure_unit: str, data: pd.DataFrame, column: str
) -> pd.DataFrame:
    if air_pressure_unit in KILO_PASCAL:
        data[column] = data[column].apply(pressure_kp_to_hpa)
    elif air_pressure_unit in MILIBAR:
        data[column] = data[column].apply(pressure_mb_to_hpa)


def normalize_radiation(
    radiation_unit: str, data: pd.DataFrame, column: str, period_seconds: int
) -> pd.DataFrame:
    if radiation_unit in MEGA_WATTS_PER_SQUARE_METER:
        data[column] = data[column].apply(radiation_MWm2_to_Wm2)
    elif radiation_unit in MEGA_JOULES_PER_SQUARE_METER:
        data[column] = data[column].apply(radiation_MJm2_to_Wm2, args=(period_seconds))
    elif radiation_unit in JOULES_PER_SQUARE_METER:
        data[column] = data[column].apply(radiation_Jm2_to_Wm2, args=(period_seconds))


def normalize_orientation(
    orientation_unit: str, data: pd.DataFrame, column: str
) -> pd.DataFrame:
    if orientation_unit in DEGREES:
        data[column] = data[column].apply(winddir_deg_to_compass)
    if orientation_unit in ES_COMPASS:
        data[column] = data[column].apply(es_compass_to_en_compass)


def normalize_timerange(
    timerange_unit: str, data: pd.DataFrame, column: str
) -> pd.DataFrame:
    if timerange_unit in HOURS:
        data[column] = data[column].apply(
            lambda x: x * 60 if x and not np.isnan(x) else None
        )


def group_and_normalize_units(
    data: pd.DataFrame, normalize_fn, group_by_column: str, data_column: str, *args
) -> pd.DataFrame:
    df_groupped = data.groupby(group_by_column, as_index=False)
    parsed_df = pd.DataFrame()
    for unit, group_df in df_groupped:
        normalize_fn(unit, group_df, data_column, *args)
        parsed_df = pd.concat([parsed_df, group_df])
    return parsed_df.sort_index()


def normalize_units_columnar(
    data: pd.DataFrame, period_seconds: int = None
) -> pd.DataFrame:
    parsed_data = data.copy()

    for column in [
        c for c in parsed_data.columns if "unit" not in c and "_node" not in c
    ]:
        if ("temperature" in column or "temp" in column) and "ground" not in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_temperature, "temperature_unit", column
            )
        if ("temperature" in column or "temp" in column) and "ground" in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_temperature, "ground_temperature_unit", column
            )
        if "dew_point" in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_temperature, "dew_point_unit", column
            )
        if "wind" in column and "dir" not in column and "direction" not in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_wind, "wind_unit", column
            )
        if "pressure" in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_pressure, "air_pressure_unit", column
            )
        if "radiation" in column or "rad" in column:
            parsed_data = group_and_normalize_units(
                parsed_data,
                normalize_radiation,
                "radiation_unit",
                column,
                [period_seconds],
            )
        if "wind" in column and ("dir" in column or "direction" in column):
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_orientation, "wind_dir_unit", column
            )
        if "leave_humidity" in column:
            parsed_data = group_and_normalize_units(
                parsed_data, normalize_timerange, "leave_humidity_unit", column
            )

    return parsed_data


def normalize_node_units_columnar(
    node_data: pd.DataFrame, data: pd.DataFrame, period_seconds: int = None
) -> pd.DataFrame:
    for column in [c for c in data.columns if "unit" not in c and "_node" not in c]:
        if ("temperature" in column or "temp" in column) and "ground" not in column:
            normalize_temperature(node_data["temperature_unit"], data, column)
        if ("temperature" in column or "temp" in column) and "ground" in column:
            normalize_temperature(node_data["ground_temperature_unit"], data, column)
        if "dew_point" in column:
            normalize_temperature(node_data["dew_point_unit"], data, column)
        if "wind" in column and "dir" not in column and "direction" not in column:
            normalize_wind(node_data["wind_unit"], data, column)
        if "pressure" in column:
            normalize_pressure(node_data["air_pressure_unit"], data, column)
        if "radiation" in column or "rad" in column:
            normalize_radiation(
                node_data["radiation_unit"], data, column, period_seconds
            )
        if "wind" in column and ("dir" in column or "direction" in column):
            normalize_orientation(node_data["wind_dir_unit"], data, column)
        if "leave_humidity" in column:
            normalize_timerange(node_data["leave_humidity_unit"], data, column)

    return data


def normalize_node_units_list(
    node_data: dict, data: list, period_seconds: int = None
) -> dict:
    return normalize_node_units_columnar(
        node_data, pd.DataFrame(data), period_seconds=period_seconds
    ).to_dict("records")


def normalize_node_units_dict(
    node_data: dict, data: dict, period_seconds: int = None
) -> dict:
    return normalize_node_units_list(node_data, [data], period_seconds).pop()


def radiation_MWm2_to_Wm2(rad_MWm2):
    return rad_MWm2 * 1000000


def radiation_Jm2_to_Wm2(rad_Jm2, period_seconds):
    return rad_Jm2 / period_seconds


def radiation_MJm2_to_Wm2(rad_MJm2, period_seconds):
    return radiation_Jm2_to_Wm2(rad_MJm2 * 1000000, period_seconds)


def es_compass_to_en_compass(wind_dir_es_compass):
    if not wind_dir_es_compass:
        return None

    es_compass = [
        "Norte",
        "NorNorEste",
        "NorEste",
        "EsteNorEste",
        "Este",
        "EsteSurEste",
        "SurEste",
        "SurSurEste",
        "Sur",
        "SurSurOeste",
        "SurOeste",
        "OesteSurOeste",
        "Oeste",
        "OesteNorOeste",
        "NorOeste",
        "NorNorOeste",
    ]

    en_compass = [
        "N",
        "NNE",
        "NE",
        "ENE",
        "E",
        "ESE",
        "SE",
        "SSE",
        "S",
        "SSW",
        "SW",
        "WSW",
        "W",
        "WNW",
        "NW",
        "NNW",
    ]

    return en_compass[es_compass.index(wind_dir_es_compass)]


def winddir_deg_to_compass(wind_dir_deg):
    if not wind_dir_deg or np.isnan(wind_dir_deg):
        return None

    val = floor((wind_dir_deg / 22.5) + 0.5)
    compass = [
        "N",
        "NNE",
        "NE",
        "ENE",
        "E",
        "ESE",
        "SE",
        "SSE",
        "S",
        "SSW",
        "SW",
        "WSW",
        "W",
        "WNW",
        "NW",
        "NNW",
    ]
    return compass[(val % 16)]


def temp_f_to_c(temp_f):
    if not temp_f or np.isnan(temp_f):
        return None

    return (temp_f - 32) * 5 / 9


def temp_k_to_c(temp_k):
    if not temp_k or np.isnan(temp_k):
        return None

    return temp_k - 273.15


def speed_mps_to_kmh(speed_mps):
    if not speed_mps or np.isnan(speed_mps):
        return None

    return speed_mps * 3.6


def speed_mph_to_kmh(speed_mph):
    if not speed_mph or np.isnan(speed_mph):
        return None

    return speed_mph * 1.60934


def speed_fps_to_kmh(speed_fps):
    if not speed_fps or np.isnan(speed_fps):
        return None

    return speed_fps * 1.09728


def speed_kts_to_kmh(speed_kts):
    if not speed_kts or np.isnan(speed_kts):
        return None

    return speed_kts * 1.852


def pressure_mb_to_hpa(pressure_mb):
    if not pressure_mb or np.isnan(pressure_mb):
        return None

    return pressure_mb


def pressure_kp_to_hpa(pressure_kp):
    if not pressure_kp or np.isnan(pressure_kp):
        return None

    return pressure_kp * 10


def replace_nans_cond(e):
    if e is pd.np.nan or (isinstance(e, float) and isnan(e)):
        e = None
    return e


def replace_nans_dict(data_list):
    for e in data_list:
        for k in e:
            e[k] = replace_nans_cond(e[k])
    return data_list


def replace_nans_list(data_list):
    for e in data_list:
        for k, _ in enumerate(e):
            e[k] = replace_nans_cond(e[k])
    return data_list


def split_dataframe(df, n):
    """
    Helper function that splits a DataFrame to a list of DataFrames of size n

    :param df: pd.DataFrame
    :param n: int
    :return: list of pd.DataFrame
    """
    n = int(n)
    df_size = len(df)
    batches = range(0, ceil((df_size / n + 1) * n), n)
    return [df.iloc[i : i + n] for i in batches if i != df_size]


def clean_dict_list_db_dups(
    dict_list,
    tablename,
    engine,
    dup_cols=[],
    filter_continuous_col=None,
    filter_categorical_col=None,
):
    return clean_df_db_dups(
        pd.DataFrame(dict_list),
        tablename,
        engine,
        dup_cols=dup_cols,
        filter_continuous_col=filter_continuous_col,
        filter_categorical_col=filter_categorical_col,
    ).to_dict("records")


def clean_df_db_dups(
    source_df,
    tablename,
    engine,
    dup_cols=[],
    filter_continuous_col=None,
    filter_categorical_col=None,
):
    """
    Remove rows from a dataframe that already exist in a database
    Required:
        source_df : dataframe to remove duplicate rows from
        engine: SQLAlchemy engine object
        tablename: tablename to check duplicates in
        dup_cols: list or tuple of column names to check for duplicate row values
    Optional:
        filter_continuous_col: the name of the continuous data column for BETWEEEN min/max filter
                               can be either a datetime, int, or float data type
                               useful for restricting the database table size to check
        filter_categorical_col : the name of the categorical data column for Where = value check
                                 Creates an "IN ()" check on the unique values in this column
    Returns
        Unique list of values from dataframe compared to database table
    """
    args = "SELECT %s FROM %s" % (
        ", ".join(["{0}".format(col) for col in dup_cols]),
        tablename,
    )
    args_contin_filter, args_cat_filter = None, None
    if (
        filter_continuous_col is not None
        and not pd.isnull(source_df[filter_continuous_col].min())
        and not pd.isnull(source_df[filter_continuous_col].max())
    ):
        if source_df[filter_continuous_col].dtype == "datetime64[ns]":
            args_contin_filter = """ %s BETWEEN '%s' AND '%s'""" % (
                filter_continuous_col,
                source_df[filter_continuous_col].min(),
                source_df[filter_continuous_col].max(),
            )

    if (
        filter_categorical_col is not None
        and len(source_df[filter_categorical_col].unique()) > 0
    ):
        args_cat_filter = " %s in(%s)" % (
            filter_categorical_col,
            ", ".join(
                [
                    "'{0}'".format(value)
                    for value in source_df[filter_categorical_col].unique()
                ]
            ),
        )

    if args_contin_filter and args_cat_filter:
        args += " Where " + args_contin_filter + " AND" + args_cat_filter
    elif args_contin_filter:
        args += " Where " + args_contin_filter
    elif args_cat_filter:
        args += " Where " + args_cat_filter

    source_df.drop_duplicates(dup_cols, keep="last", inplace=True)
    # print('Clean dups SQL: ' + args)
    source_df_existing = pd.read_sql(args, engine)
    print("{} dups to clean".format(len(source_df_existing.index)))

    source_df = source_df.merge(
        source_df_existing, how="left", on=dup_cols, indicator=True
    )
    # print('Merged duped {}'.format(source_df[source_df['_merge'] == 'left_only'].head(10)))
    source_df = source_df[source_df["_merge"] == "left_only"]
    source_df.drop(["_merge"], axis=1, inplace=True)
    return source_df
