import logging
import sqlalchemy as sql

from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker
from sqlalchemy import exc
from sqlalchemy.orm.session import Session


logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class RDSClient:
    """Client for RDS connections"""

    def __init__(self, rds_host, db_name, username, password):
        # TODO: add ssh tunnel connection option
        self._rds_host = rds_host
        self._db_name = db_name
        self._username = username
        self._password = password

        self._sql_engine = None
        self._connection = None
        self._metadata = None
        self._session = None

        self.open_connection()

    def get_engine(self):
        return self._sql_engine

    def connect(self):
        connect_string = f"mysql+pymysql://{self._username}:{self._password}@{self._rds_host}/{self._db_name}"

        self._sql_engine = sql.create_engine(
            connect_string, pool_size=1, pool_pre_ping=True
        )
        self._connection = self._sql_engine.connect()
        self._metadata = sql.MetaData()
        self._session: Session = sessionmaker(bind=self._sql_engine, autocommit=False)

    def open_connection(self):
        try:
            # print("Opening Connection")
            if not self._connection or self._connection.closed:
                self.connect()

        except Exception as e:
            logger.error(e)
            logger.error(
                "ERROR: Unexpected error: Could not connect to MySql instance."
            )
            raise e

    def execute(self, statement, query_parameters=None):
        res = None
        with self.session_scope() as session:
            res = session.execute(
                statement, {**query_parameters} if query_parameters else None
            )
        return res

    @contextmanager
    def session_scope(self):
        """Provide a transactional scope around a series of operations."""
        session: Session = self._session()
        try:
            yield session
            session.commit()
        except exc.ProgrammingError as e:
            logger.error(f"ProgrammingError: {e}")
            session.rollback()
        except exc.SQLAlchemyError as e:
            logger.error(f"SQLAlchemyError: {e}")
            session.rollback()
        finally:
            session.close()
