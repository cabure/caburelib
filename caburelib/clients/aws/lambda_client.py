import boto3

client = boto3.client("lambda")


class LambdaClient:
    """Client for Lambda API."""

    _fnName = None

    def __init__(self, fnName):
        self._fnName = fnName

    def invoke_fn(self, payload):
        response = client.invoke(
            FunctionName=self._fnName, InvocationType="Event", Payload=payload.encode()
        )

        return response
