import json

import boto3

CLIENT = boto3.client("stepfunctions")


class SFNClient:
    """Client for SFN API."""

    _state_machine_arn = None

    def __init__(self, state_machine_arn):
        self._state_machine_arn = state_machine_arn

    def start_execution(self, name, input_data):
        response = CLIENT.start_execution(
            stateMachineArn=self._state_machine_arn,
            name=name,
            input=json.dumps(input_data),
        )

        return response