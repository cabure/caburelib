import ast
import boto3
import gzip
import json
import logging
import math
import pytz

from botocore.exceptions import ClientError
from datetime import datetime
from io import BytesIO

from ...utils import json_to_jsonl, jsonl_to_json


s3 = boto3.client("s3")
logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class S3Client:
    """Client for S3 API."""

    _bucket = None

    def __init__(self, bucket):
        self._bucket = bucket

    def delete_object(self, key):
        try:
            s3.delete_object(Bucket=self._bucket, Key=key)
        except ClientError as ex:
            if ex.response["Error"]["Code"] == "NoSuchKey":
                logger.info(f"No object found for key {key}")
                return False
            else:
                raise (ex)

        return True

    def put_data_s3(
        self,
        dest_object_name,
        src_data,
        content_type="text/plain",
        content_encoding="utf-8",
    ):
        if isinstance(src_data, bytes):
            object_data = src_data
        elif isinstance(src_data, str):
            try:
                object_data = open(src_data, "rb")
            except Exception as e:
                logger.exception(e)
                return False
        else:
            logger.error(
                "Type of "
                + str(type(src_data))
                + " for the argument 'src_data' is not supported."
            )
            return False

        try:
            logger.info("inserting object {} into S3...".format(dest_object_name))
            s3.put_object(
                Bucket=self._bucket,
                Key=dest_object_name,
                Body=object_data,
                ContentType=content_type,
                ContentEncoding=content_encoding,
            )
        finally:
            if isinstance(src_data, str):
                object_data.close()
        return True

    def object_age_seconds(self, object_name):
        try:
            data = s3.head_object(Bucket=self._bucket, Key=object_name)
            return (datetime.utcnow().replace(tzinfo=pytz.UTC) - data["LastModified"]).total_seconds()
        except ClientError as e:
            logger.exception(e)
            return math.inf


    def object_exists(self, object_name):
        try:
            s3.head_object(Bucket=self._bucket, Key=object_name)
        except ClientError as e:
            if (
                e.response["Error"]["Code"] == "404"
                or e.response["Error"]["Code"] == "NoSuchKey"
            ):
                return False
            else:
                raise
        else:
            return True

    def get_dict(self, object_name, gzipped=False):
        obj = (
            self.get_object_gzipped(object_name)
            if gzipped
            else self.get_object(object_name)
        )

        if obj is None:
            raise FileNotFoundError(
                f"Object {object_name} not found in {self._bucket}."
            )

        return ast.literal_eval(obj.read().decode())

    def get_json(self, object_name, gzipped=False):
        obj = (
            self.get_object_gzipped(object_name)
            if gzipped
            else self.get_object(object_name)
        )

        if obj is None:
            raise FileNotFoundError(
                f"Object {object_name} not found in {self._bucket}."
            )
        return json.load(obj)

    def get_jsonl(self, object_name, gzipped=True):
        jsonl_data = (
            self.get_object_gzipped(object_name)
            if gzipped
            else self.get_object(object_name)
        )

        if jsonl_data is None:
            raise FileNotFoundError(
                f"Object {object_name} not found in {self._bucket}."
            )

        return jsonl_to_json(jsonl_data)

    def get_object(self, object_name):
        try:
            response = s3.get_object(Bucket=self._bucket, Key=object_name)
        except ClientError as ex:
            if ex.response["Error"]["Code"] == "NoSuchKey":
                logger.info(f"No object found for key {object_name}")
                return None
            else:
                raise ex
        # Return an open StreamingBody object
        return response["Body"]

    def get_object_gzipped(self, object_name):
        obj = self.get_object(object_name)

        if obj is None:
            return None

        compressed_fp = BytesIO(obj.read())
        compressed_fp.seek(0)
        return gzip.GzipFile(None, "rb", fileobj=compressed_fp)

    def put_jsonl(
        self,
        dest_object_name: str,
        data: list,
        concatenate: bool = False,
        compressed: bool = True,
    ):
        # TODO: establish max_size concat
        return (
            self.put_data_gzipped(
                dest_object_name,
                json_to_jsonl(data),
                content_type="application/jsonl",
                concatenate=concatenate,
            )
            if compressed
            else self.put_data_s3(  # TODO: add concatenate
                dest_object_name, json_to_jsonl(data), content_type="application/json"
            )
        )

    def put_data_gzipped(
        self,
        dest_object_name: str,
        data: bytes,
        content_type: str = "text/plain",
        concatenate: bool = False,
        max_size: int = None,
    ) -> bool:
        prev_data = b""
        if concatenate:
            prev_data_obj = self.get_object_gzipped(dest_object_name)
            if prev_data_obj is not None:
                prev_data = prev_data_obj.read()

        compressed_fp = BytesIO()
        with gzip.GzipFile(fileobj=compressed_fp, mode="wb") as gz:
            gz.write(b"".join([prev_data, data]))

        compressed_fp.seek(0)
        return self.put_data_s3(
            dest_object_name, compressed_fp.read(), content_type, "gzip"
        )

    def iterate_bucket_filtered_items(self, prefix_filter=None, suffix_filter=None):
        """
        Generator that iterates over all objects in a given s3 bucket

        :param prefix_filter: prefix filter for key
        :param suffix_filter: suffix filter for key
        :return: dict of metadata for an object
        """

        paginator = s3.get_paginator("list_objects_v2")

        kwargs = {"Bucket": self._bucket}
        if isinstance(prefix_filter, str):
            kwargs["Prefix"] = prefix_filter

        page_iterator = paginator.paginate(**kwargs)

        for page in page_iterator:
            if page["KeyCount"] > 0:
                for item in page["Contents"]:
                    if (
                        prefix_filter is None or item["Key"].startswith(prefix_filter)
                    ) and (
                        suffix_filter is None or item["Key"].endswith(suffix_filter)
                    ):
                        yield item
