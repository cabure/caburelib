import boto3

client = boto3.client("sqs")


class SQSClient:
    """Client for SQS API."""

    _queueURL = None
    _delay = None

    def __init__(self, queueURL, delay=10):
        self._queueURL = queueURL
        self._delay = delay

    def send_message_batch(self, messages):
        response = client.send_message_batch(
            QueueUrl=self._queueURL,
            Entries=[
                {
                    "Id": message.Id,
                    "MessageBody": (message.body),
                    "DelaySeconds": self._delay,
                    "MessageAttributes": message.attributes,
                }
                for message in messages
            ],
        )

        return response

    def send_message(self, attributes, body):
        response = client.send_message(
            QueueUrl=self._queueURL,
            DelaySeconds=self._delay,
            MessageAttributes=attributes,
            MessageBody=(body),
        )

        return response

    def delete_message(self, receipt_handle):
        response = client.delete_message(
            QueueUrl=self._queueURL, ReceiptHandle=receipt_handle
        )

        return response
