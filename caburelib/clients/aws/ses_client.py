import boto3
import os

from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart


class SESClient(object):
    """Client for SES API."""

    def __init__(self, region_name):
        self._client = boto3.client("ses", region_name=region_name)

    def send_email(
        self,
        to,
        subject,
        fromx,
        reply_to=None,
        body=None,
        content=None,
        attachments=None,
        is_html=True,
    ):
        """ sends email with attachments
        
        Parameters:
            * to (list or comma separated string of addresses): recipient(s) address
            * fromx (string): from address of email
            * body (string, optional): Body of email ('\n' are converted to '< br/>')
            * content (string, optional): Body of email specified as filename
            * attachments (list, optional): list of paths of files to attach
        """

        if attachments is None:
            attachments = []

        self.to = to
        self.reply_to = reply_to
        self.subject = subject
        self.fromx = fromx
        self.attachment = None
        self.body = body
        self.content = content
        self.attachments = attachments

        if type(self.to) is list:
            self.to = ",".join(self.to)

        if type(self.reply_to) is list:
            self.reply_to = ",".join(self.reply_to)

        message = MIMEMultipart()

        message["Subject"] = self.subject
        message["From"] = self.fromx
        message["To"] = self.to
        if self.reply_to:
            message["Reply-To"] = self.reply_to
        if self.content and os.path.isfile(self.content):
            content_text = open(str(self.content)).read()
            if not is_html:
                content_text = content_text.replace("\n", "<br />")
            part = MIMEText(content_text, "html")
            message.attach(part)
        elif self.body:
            if not is_html:
                self.body = self.body.replace("\\n", "<br />").replace("\n", "<br />")
            part = MIMEText(self.body, "html")
            message.attach(part)

        for attachment in self.attachments:
            if os.path.isfile(attachment):
                attachment_bytes = open(attachment, "rb").read()
            else:
                attachment_bytes = attachment

            part = MIMEApplication(attachment_bytes)
            part.add_header(
                "Content-Disposition", "attachment", filename=attachment.split("/")[-1]
            )
            message.attach(part)

        response = self._client.send_raw_email(
            Source=message["From"],
            Destinations=message["To"].split(","),
            RawMessage={"Data": message.as_string()},
        )

        return response
