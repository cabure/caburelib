import boto3


class DynamoClient:
    """Client for DynamoDB API."""

    _dynamodb = None
    _table = None

    def __init__(self, table):
        self._dynamodb = boto3.resource("dynamodb")
        self._table = self._dynamodb.Table(table)

    def get_item(self, key, col):
        value = self._table.get_item(Key=key)

        res = None
        if value and "Item" in value and col in value["Item"]:
            res = value["Item"][col]

        return res

    def update_item(self, key, update_expression, attribute_values, return_values):
        return self._table.update_item(
            Key=key,
            UpdateExpression=update_expression,
            ExpressionAttributeValues=attribute_values,
            ReturnValues=return_values,
        )
