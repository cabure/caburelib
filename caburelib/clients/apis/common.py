POST = "POST"
GET = "GET"


class APIError(Exception):
    """An API Error Exception"""

    def __init__(self, status, message=None):
        self.status = status
        self.message = message

    def __str__(self):
        return "APIError: status={}".format(self.status)