import logging
import requests

from enum import Enum

from .common import APIError, GET, POST

DATE_FORMAT = "%Y-%m-%d"

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class SiarDataScopes(Enum):
    CCAA = 1
    Provincia = 2
    Estacion = 3


class SiarDataTypes(Enum):
    Horarios = 1
    Diarios = 2
    Semanales = 3
    Mensuales = 4


class SiarClient:
    """Client for Siar API."""

    def __init__(self, api_key):
        self._apiRoute = "https://servicio.mapama.gob.es/apisiar/API/v1"
        self._api_key = api_key
        self._session = requests.Session()

    def _get(self, method, query_params=None):
        query_params_string = (
            "&".join(
                [
                    f"{query_param['key']}={query_param['value']}"
                    for query_param in query_params
                    if query_param["value"]
                ]
            )
            if query_params
            else ""
        )
        url = f"{self._apiRoute}{method}?{query_params_string}&ClaveAPI={self._api_key}"
        resp = self._session.get(url, timeout=30)

        body = resp.json()

        if resp.status_code != 200:
            msg = f"GET {url} {resp.status_code} {body['MensajeRespuesta'] if 'MensajeRespuesta' in body else ''}"
            logging.error(msg)
            raise APIError(resp.status_code, msg)

        if not body["Datos"]:
            msg = f"No data returned in response: {body['MensajeRespuesta'] if 'MensajeRespuesta' in body else ''}"
            logging.error(msg)
            raise APIError(404, msg)

        logging.debug(f"GET {url} responded {body}")
        return body["Datos"]

    def _get_data(
        self, data_type, data_scope, scope_id, date_from, date_to, date_last_update=None
    ):
        return self._get(
            f"/datos/{data_type.name}/{data_scope.name}",
            [
                {"key": "Id", "value": scope_id},
                {"key": "FechaInicial", "value": date_from.strftime(DATE_FORMAT)},
                {"key": "FechaFinal", "value": date_to.strftime(DATE_FORMAT)},
                {
                    "key": "FechaUltModificacion",
                    "value": date_last_update.strftime(DATE_FORMAT)
                    if date_last_update
                    else None,
                },
            ],
        )

    def usage_info(self):
        return self._get(f"/info/Accesos")

    def list_communities(self):
        return self._get(f"/info/CCAA")

    def list_provinces(self):
        return self._get(f"/info/Provincias")

    def list_stations(self):
        return self._get(f"/info/Estaciones")

    def hourly_data_by_station(
        self, station_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Horarios,
            SiarDataScopes.Estacion,
            station_id,
            date_from,
            date_to,
            date_last_update,
        )

    def daily_data_by_station(
        self, station_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Diarios,
            SiarDataScopes.Estacion,
            station_id,
            date_from,
            date_to,
            date_last_update,
        )

    def weekly_data_by_station(
        self, station_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Semanales,
            SiarDataScopes.Estacion,
            station_id,
            date_from,
            date_to,
            date_last_update,
        )

    def monthly_data_by_station(
        self, station_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Mensuales,
            SiarDataScopes.Estacion,
            station_id,
            date_from,
            date_to,
            date_last_update,
        )

    def hourly_data_by_province(
        self, province_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Horarios,
            SiarDataScopes.Provincia,
            province_id,
            date_from,
            date_to,
            date_last_update,
        )

    def daily_data_by_province(
        self, province_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Diarios,
            SiarDataScopes.Provincia,
            province_id,
            date_from,
            date_to,
            date_last_update,
        )

    def weekly_data_by_province(
        self, province_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Semanales,
            SiarDataScopes.Provincia,
            province_id,
            date_from,
            date_to,
            date_last_update,
        )

    def monthly_data_by_province(
        self, province_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Mensuales,
            SiarDataScopes.Provincia,
            province_id,
            date_from,
            date_to,
            date_last_update,
        )

    def hourly_data_by_community(
        self, community_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Horarios,
            SiarDataScopes.CCAA,
            community_id,
            date_from,
            date_to,
            date_last_update,
        )

    def daily_data_by_community(
        self, community_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Diarios,
            SiarDataScopes.CCAA,
            community_id,
            date_from,
            date_to,
            date_last_update,
        )

    def weekly_data_by_community(
        self, community_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Semanales,
            SiarDataScopes.CCAA,
            community_id,
            date_from,
            date_to,
            date_last_update,
        )

    def monthly_data_by_community(
        self, community_id, date_from, date_to, date_last_update=None
    ):
        return self._get_data(
            SiarDataTypes.Mensuales,
            SiarDataScopes.CCAA,
            community_id,
            date_from,
            date_to,
            date_last_update,
        )
