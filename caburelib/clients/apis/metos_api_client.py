import requests
import logging

from requests.auth import AuthBase
from Crypto.Hash import HMAC
from Crypto.Hash import SHA256
from datetime import datetime

from .common import APIError

# Class to perform HMAC encoding

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class AuthHmacMetosGet(AuthBase):
    # Creates HMAC authorization header for Metos REST service GET request.
    def __init__(self, apiRoute, publicKey, privateKey):
        self._publicKey = publicKey
        self._privateKey = privateKey
        self._method = "GET"
        self._apiRoute = apiRoute

    def __call__(self, request):
        dateStamp = datetime.utcnow().strftime("%a, %d %b %Y %H:%M:%S GMT")
        request.headers["Date"] = dateStamp
        msg = (self._method + self._apiRoute + dateStamp + self._publicKey).encode(
            encoding="utf-8"
        )
        h = HMAC.new(self._privateKey.encode(encoding="utf-8"), msg, SHA256)
        signature = h.hexdigest()
        request.headers["Authorization"] = "hmac " + self._publicKey + ":" + signature
        return request


class MetosClient:
    """Client for Metos API."""

    # HMAC Authentication credentials
    _publicKey = None
    _privateKey = None

    _apiURI = None

    def __init__(self, apiURI, publicKey, privateKey):
        self._publicKey = publicKey
        self._privateKey = privateKey
        self._apiURI = apiURI

    def get(self, route):
        auth = AuthHmacMetosGet(route, self._publicKey, self._privateKey)
        url = self._apiURI + route
        response = requests.get(
            url, headers={"Accept": "application/json"}, auth=auth, timeout=30
        )

        if response.status_code == 204:
            logger.info("GET {} responded empty".format(url))
            return None

        if response.status_code != 200:
            msg = "GET {url} {code}".format(url=url, code=response.status_code)
            logger.error(msg)
            raise APIError(response.status_code, msg)

        logger.info("GET {} responded".format(url))
        return response.json()

    def get_stations(self):
        return self.get("/user/stations")

    def get_station_information(self, station_id):
        return self.get("/station/{}".format(station_id))

    def get_station_sensors(self, station_id):
        return self.get("/station/{}/sensors".format(station_id))

    def get_station_data(self, station_id, timestamp_from, timestamp_to):
        return self.get(
            f"/data/optimized/{station_id}/raw/from/{timestamp_from}/to/{timestamp_to}"
        )
