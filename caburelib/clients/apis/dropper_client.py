import requests
import logging

from .common import APIError
from ...utils import decrypt_pass

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class DropperClient:
    """Client for Dropper API."""

    def __init__(self, api_route, user, password, enc_key):
        self._token = None
        self._api_route = api_route
        self._user = user
        self._password = decrypt_pass(enc_key, password)
        self._session = requests.Session()

    def _post(self, json_body, query_params=None):
        url = self._api_route + ("?" + query_params if query_params else "")
        resp = self._session.post(url, json=json_body, timeout=30)

        if resp.status_code != 200:
            msg = "POST {url} {body} {code}".format(
                url=url, body=str(json_body), code=resp.status_code
            )
            logger.error(msg)
            raise APIError(resp.status_code, msg)

        body = resp.json()
        if "pass" in json_body:
            json_body["pass"] = "xxxxx"

        logger.debug(f"POST {url} {str(json_body)} responded {len(body)} rows")
        return body

    def _authorize(self):
        response = self._post(
            {"op": "valid", "user": self._user, "pass": self._password}
        )
        if response is None or "token" not in response:
            msg = (
                "No token returned in response data"
                if response is None or "status" not in response
                else response["status"]
            )
            logger.error(msg)
            raise ValueError(msg)

        self._token = response["token"]
        return

    def last_data(self, station_id):
        if self._token is None:
            self._authorize()
        response = self._post({"op": "status", "token": self._token, "id": station_id})

        if not isinstance(response, list):
            msg = (
                "No data returned in response"
                if response is None or "status" not in response
                else response["status"]
            )
            logger.error(msg)
            raise ValueError(msg)

        return response

    def period_data(self, station_id, days):
        if self._token is None:
            self._authorize()

        allowed_days = [0, 1, 7, 30, 90]
        days_param = min(allowed_days, key=lambda x: abs(x - days))
        response = self._post(
            {
                "op": "periodo",
                "token": self._token,
                "id": station_id,
                "dias": days_param,
            }
        )

        if not isinstance(response, list):
            msg = (
                "No data returned in response"
                if response is None or "status" not in response
                else response["status"]
            )
            logger.error(response)
            logger.error(msg)
            raise ValueError(msg)

        return response
