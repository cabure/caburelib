import logging
import requests
import json

from datetime import datetime
from typing import List, Optional

from .common import APIError, GET, POST

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class InmetStation:
    """Inmet Station object"""

    CD_ESTACAO: str
    CD_OSCAR: Optional[str]
    DC_NOME: str
    FL_CAPITAL: Optional[str]
    DT_FIM_OPERACAO: Optional[datetime]
    CD_SITUACAO: str
    TP_ESTACAO: str
    VL_LATITUDE: float
    VL_LONGITUDE: float
    VL_ALTITUDE: Optional[float]
    CD_WSI: Optional[str]
    CD_DISTRITO: str
    SG_ESTADO: str
    SG_ENTIDADE: str
    DT_INICIO_OPERACAO: datetime


class InmetClient:
    """
    Client for Inmet Brazil API.

    ## ATRIBUTOS DIARIOS

    | CAMPO         | DESCRIÇÃO                                   | UNIDADE |
    | ------------- | ------------------------------------------- | ------- |
    | CHUVA         | PRECIPITAÇÃO TOTAL, DIARIO (AUT)            | mm      |
    | PRESS_ATM_MED | PRESSAO ATMOSFERICA MEDIA DIARIA (AUT)      | mB      |
    | TEMP_MAX      | TEMPERATURA MÁXIMA, DIARIA (AUT)            | °C      |
    | TEMP_MED      | TEMPERATURA MÉDIA, DIÁRIA (AUT)             | °C      |
    | TEMP_MIN      | TEMPERATURA MÍNIMA, DIARIA (AUT)            | °C      |
    | UMID_MED      | UMIDADE RELATIVA DO AR, MEDIA DIARIA (AUT)  | %       |
    | UMID_MIN      | UMIDADE RELATIVA DO AR, MINIMA DIARIA (AUT) | %       |
    | VEL_VENTO_MED | VENTO, VELOCIDADE MÉDIA DIÁRIA (AUT)        | m/s     |

    ## ATRIBUTOS HORARIOS

    | CODIGO ATRIBUTO | DESCRIÇÃO                                        | UNIDADE |
    | --------------- | ------------------------------------------------ | ------- |
    | VEN_DIR         | VENTO, DIREÇÃO HORARIA (gr)                      | ° (gr)  |
    | CHUVA           | PRECIPITAÇÃO TOTAL, HORÁRIO                      | mm      |
    | PRE_INS         | PRESSÃO ATMOSFÉRICA AO NÍVEL DA ESTACÃO, HORARIA | mB      |
    | PRE_MIN         | PRESSÃO ATMOSFÉRICA MIN. NA HORA ANT. (AUT)      | mB      |
    | UMD_MAX         | UMIDADE REL. MAX. NA HORA ANT. (AUT)             | %       |
    | PRE_MAX         | PRESSÃO ATMOSFÉRICA MAX.NA HORA ANT. (AUT)       | mB      |
    | VEN_VEL         | VENTO, VELOCIDADE HORARIA                        | m/s     |
    | PTO_MIN         | TEMPERATURA ORVALHO MIN. NA HORA ANT. (AUT)      | °C      |
    | TEM_MAX         | TEMPERATURA MÁXIMA NA HORA ANT. (AUT)            | °C      |
    | RAD_GLO         | RADIAÇÃO GLOBAL                                  | W/m²    |
    | PTO_INS         | TEMPERATURA DO PONTO DE ORVALHO                  | °C      |
    | VEN_RAJ         | VENTO, RAJADA MÁXIMA                             | m/s     |
    | TEM_INS         | TEMPERATURA DO AR - BULBO SECO, HORARIA          | °C      |
    | UMD_INS         | UMIDADE RELATIVA DO AR, HORARIA                  | %       |
    | TEM_MIN         | TEMPERATURA MÍNIMA NA HORA ANT. (AUT)            | °C      |
    | UMD_MIN         | UMIDADE REL. MIN. NA HORA ANT. (AUT)             | %       |
    | PTO_MAX         | TEMPERATURA ORVALHO MAX. NA HORA ANT. (AUT)      | °C      |

    """

    _api_route = "https://apitempo.inmet.gov.br/"

    def __init__(self):
        self._session = requests.Session()

    def _request(self, verb, method, json_body=None, query_params=None):
        verb = verb.upper()

        query_string = (
            "&".join([f"{key}={value}" for key, value in query_params.items()])
            if query_params
            else None
        )
        url = f'{self._api_route}{method}{f"/?{query_string}" if query_string else ""}'

        kwargs = {"url": url, "json": json_body}

        logger.debug(kwargs)

        resp = None
        if verb == POST:
            resp = self._session.post(timeout=30, **kwargs)
        if verb == GET:
            resp = self._session.get(timeout=30, **kwargs)

        try:
            body = resp.json()
        except json.decoder.JSONDecodeError as e:
            body = resp.text
            logger.warn(f"API responded: {resp.text}")

        if body == "Você atingiu o limite de requisições.":
            raise APIError(429, body)

        if body is None:
            msg = "Empty response"
            logger.error(msg)
            raise APIError(400, msg)

        if resp.status_code != 200:
            msg = f"{verb} {url} {str(json_body)} {resp.status_code} {body}"
            logger.error(msg)
            raise APIError(resp.status_code, msg)

        logger.debug(f"{verb} {url} {str(json_body)} returned {len(body)} rows")

        return body

    def get_manual_stations(self) -> List[dict]:
        response = self._request(GET, "/estacoes/M")

        if len(response) == 0:
            msg = "No manual stations found"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_automatic_stations(self) -> List[dict]:
        response = self._request(GET, "/estacoes/T")

        if len(response) == 0:
            msg = "No automatic stations found"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_all_stations(self) -> List[dict]:
        try:
            manual = self.get_manual_stations()
        except APIError as e:
            if e.status == 404:
                manual = []
            else:
                raise e
        try:
            automatic = self.get_automatic_stations()
        except APIError as e:
            if e.status == 404:
                automatic = []
            else:
                raise e

        response = manual + automatic

        if len(response) == 0:
            msg = "No stations found"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_all_data_per_day(self, data_date: datetime):
        response = self._request(GET, f"estacao/dados/{data_date.strftime('%Y-%m-%d')}")

        if len(response) == 0:
            msg = f"No data found for {data_date.strftime('%Y-%m-%d')}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_all_data_per_hour(self, data_date: datetime):
        response = self._request(
            GET,
            f"estacao/dados/{data_date.strftime('%Y-%m-%d')}/{data_date.strftime('%H00')}",
        )

        if len(response) == 0:
            msg = f"No data found for {data_date.strftime('%Y-%m-%d %H:00')}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_data(
        self, from_date: datetime, to_date: datetime, station_code: str
    ):
        response = self._request(
            GET,
            f"estacao/{from_date.strftime('%Y-%m-%d')}/{to_date.strftime('%Y-%m-%d')}/{station_code}",
        )

        if len(response) == 0:
            msg = f"No data found for station #{station_code} between {from_date.strftime('%Y-%m-%d')} and {to_date.strftime('%Y-%m-%d')}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_daily_data(
        self, from_date: datetime, to_date: datetime, station_code: str
    ):
        response = self._request(
            GET,
            f"estacao/diaria/{from_date.strftime('%Y-%m-%d')}/{to_date.strftime('%Y-%m-%d')}/{station_code}",
        )

        if len(response) == 0:
            msg = f"No data found for station #{station_code} between {from_date.strftime('%Y-%m-%d')} and {to_date.strftime('%Y-%m-%d')}"
            logger.error(msg)
            raise APIError(404, msg)

        return response