import copy
import logging
import requests

from .common import APIError, GET, POST

DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S.%f"

logger = logging.getLogger(__name__)
logger.addHandler(logging.NullHandler())


class OmixomClient:
    """Client for Omixom API."""

    _api_route = "https://new.omixom.com/api/"

    def __init__(self, user, password):
        self._user = user
        self._password = password
        self._session = requests.Session()
        self._token = None

    def _check_authed(self):
        """ Returns True if client has auth token, raises an exception if not. """

        if not self._token:
            raise APIError(403, "Authentication exception: not connected.")
        return True

    def _obfuscate_log(self, response):
        res_cpy = copy.deepcopy(response)
        if "token" in res_cpy:
            res_cpy["token"] = "xxxxx"
        if "headers" in res_cpy and "Authorization" in res_cpy["headers"]:
            res_cpy["headers"]["Authorization"] = "xxxxx"
        return res_cpy

    def _request(self, verb, method, json_body=None, authed=True, query_params=None):
        verb = verb.upper()

        if authed:
            self._check_authed()

        query_string = (
            "&".join([f"{key}={value}" for key, value in query_params.items()])
            if query_params
            else None
        )
        url = f'{self._api_route}{method}{f"/?{query_string}" if query_string else ""}'

        kwargs = {"url": url, "json": json_body}
        if authed:
            kwargs["headers"] = {"Authorization": f"Token {self._token}"}

        logger.debug(kwargs)

        resp = None
        if verb == POST:
            resp = self._session.post(timeout=30, **kwargs)
        if verb == GET:
            resp = self._session.get(timeout=30, **kwargs)

        body = resp.json()

        if body is None:
            msg = "Empty response"
            logger.error(msg)
            raise APIError(400, msg)

        if resp.status_code != 200:
            msg = f"{verb} {url} {str(json_body)} {resp.status_code} {body}"
            logger.error(msg)
            raise APIError(resp.status_code, msg)

        logger.debug(f"{verb} {url} {str(json_body)} returned {len(body)} rows")

        return body

    def authorize(self):
        response = self._request(
            POST,
            "get_auth_token/",
            {"username": self._user, "password": self._password},
            authed=False,
        )

        if "token" not in response:
            msg = "No token returned in response data"
            logger.error(msg)
            raise APIError(400, msg)

        self._token = response["token"]

    def get_stations(self):
        response = self._request(GET, "stations")

        if len(response) == 0:
            msg = "No stations found"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_sensors(self, station_code):
        response = self._request(
            GET, f"stations4module/{station_code}", query_params={"format": "json"}
        )

        if len(response) == 0:
            msg = f"No sensors found for station #{station_code}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_last_measure(self, station_code):
        response = self._request(
            GET, f"last_measure/{station_code}", query_params={"format": "json"}
        )

        if len(response) == 0:
            msg = f"No measurements found for station #{station_code}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_measurements_id(self, station_code, date_from, date_to):
        date_from_str = date_from.strftime(DATETIME_FORMAT)
        date_to_str = date_to.strftime(DATETIME_FORMAT)
        response = self._request(
            GET,
            f"measures/{station_code}",
            query_params={
                "format": "json",
                "date_from": date_from_str,
                "date_to": date_to_str,
            },
        )

        if len(response) == 0:
            msg = f"No measurements found for station #{station_code}, from {date_from_str} to {date_to_str}"
            logger.error(msg)
            raise APIError(404, msg)

        return response

    def get_station_measurements(self, station_code, date_from, date_to):
        date_from_str = date_from.strftime(DATETIME_FORMAT)
        date_to_str = date_to.strftime(DATETIME_FORMAT)
        response = self._request(
            GET,
            f"measures/wnames/{station_code}",
            query_params={
                "format": "json",
                "date_from": date_from_str,
                "date_to": date_to_str,
            },
        )

        if len(response) == 0:
            msg = f"No measurements found for station #{station_code}, from {date_from_str} to {date_to_str}"
            logger.error(msg)
            raise APIError(404, msg)

        return response
