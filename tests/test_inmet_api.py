from datetime import datetime

from caburelib.clients.apis.inmet_api_client import InmetClient

def test_get_stations():
    client = InmetClient()
    response = client.get_all_stations()

    assert response

def test_get_station_data():
    client = InmetClient()
    stations = client.get_automatic_stations()
    assert len(stations) > 0

    data = client.get_station_data(datetime(2021,5,8, 0, 1), datetime(2021,5,10,12, 20), "A414")#stations[0].get("CD_ESTACAO"))
    assert data

    daily_data = client.get_station_daily_data(datetime.now(), datetime.now(), stations[0].get("CD_ESTACAO"))
    assert daily_data

def test_get_all_stations_data():
    client = InmetClient()
    data = client.get_all_data_per_day(datetime.now())
    assert data

    client = InmetClient()
    data = client.get_all_data_per_hour(datetime.now())
    assert data