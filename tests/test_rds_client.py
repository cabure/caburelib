import sqlalchemy as sql

from sqlalchemy import and_
from sqlalchemy.orm.session import sessionmaker

import caburelib.models_rds as models


def test_rds_query():
    connect_string = "mysql+pymysql://root:@127.0.0.1/cabure_dev"

    sql_engine = sql.create_engine(connect_string, pool_size=1, pool_pre_ping=True)
    session = sessionmaker(bind=sql_engine, autocommit=False)

    query = (
        session()
        .query(models.Node)
        .join(models.GPCBANodes, models.Node.nodable_id == models.GPCBANodes.id)
        .filter(
            and_(
                models.Node.nodable_type == models.NodableTypes.GPCBA.value,
                models.Node.deleted_at.is_(None),
            )
        )
    )