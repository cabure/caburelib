"""Caburelib setup file."""
from setuptools import setup, find_packages

import caburelib

with open("README.md", "r", encoding="utf8") as fh:
    long_description = fh.read()

setup(
    name="caburelib",
    version=caburelib.__version__,
    packages=find_packages(),
    python_requires=">=3.6, <3.9",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=open("requirements.txt").read().strip().split("\n"),
)