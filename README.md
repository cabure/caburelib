## Environments

Create virtualenv:

```
virtualenv venv-caburelib
source venv-caburelib/bin/activate
```

Install deps:

```
pip install -r requirements.txt
```

### SSH Tunnel

To connect to DB through SSH tunnel;

```
from sshtunnel import SSHTunnelForwarder
from caburelib.clients.aws.rds_mysql_client import RDSClient

tunnel = SSHTunnelForwarder(
    (SSH_HOST, SSH_PORT),
    ssh_username=SSH_USER, 
    ssh_pkey=SSH_PKEY_FILE_PATH, # if needs pkey
    ssh_private_key_password=SSH_PKEY_PASSWORD, # if key is password protected
    remote_bind_address=(DB_HOST, DB_PORT)) 
tunnel.start()
rds_client = RDSClient(f'127.0.0.1:{tunnel.local_bind_port}', DB_NAME, DB_USER, DB_PASS)
```

(TODO: add this as a feature of caburelib.clients.aws.rds_mysql_client)